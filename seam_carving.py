import argparse
import cv2
import numpy as np
#from matplotlib import pyplot
import numba
import imutils
from tqdm import trange


def get_pixel_energies(image):
    # convert the image to gray
    img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # Calculate the Laplacian gradient using cv2
    energy = cv2.Laplacian(img_gray, cv2.CV_64F)
    energy = np.absolute(energy)
    # return the calculated energy_map
    return energy


@numba.jit
def calculate_minimum_seam(image, height, width):
    energy = get_pixel_energies(image)

    # create dynamic 2d array to store energy values seen up until that pixel
    min_map = energy.copy()
    back_map = np.zeros_like(min_map, dtype=np.int)

    for h in range(1, height):
        index = np.argmin(min_map[h - 1, 0:2])
        back_map[h, 0] = index
        min_energy = min_map[h - 1, index]
        min_map[h, 0] += min_energy
        for w in range(1, width):
            idx = np.argmin(min_map[h - 1, w - 1:w + 2])
            back_map[h, w] = idx + w - 1
            min_energy = min_map[h - 1, idx + w - 1]
            min_map[h, w] += min_energy
    return min_map, back_map


@numba.jit
def remove_pixels(image, height, width):
    min_map, back_map = calculate_minimum_seam(image, height, width)

    # create a [h, w] map to remove false values later
    keep_pixels_map = np.full((height, width), True)

    # get the lowest energy pixel at the bottom of the image
    lowest_energy_pixel_in_column = np.argmin(min_map[-1])

    # work backwards using backtrack to get to the top of the image.
    for rowOfPixels in reversed(range(height)):
        keep_pixels_map[rowOfPixels, lowest_energy_pixel_in_column] = False
        lowest_energy_pixel_in_column = back_map[rowOfPixels, lowest_energy_pixel_in_column]

    # copy this mask to all layers of the image (r, g, b)
    # https://stackoverflow.com/questions/26821137/masking-bgr-image-using-a-2d-mask
    mask = np.zeros((height, width, 3), dtype=np.bool)
    for i in range(3):
        mask[:, :, i] = keep_pixels_map.copy()

    # apply the mask to the image
    # remove the one column from the image
    masked_image = image[mask].reshape((height, width - 1, 3))
    return masked_image


if __name__ == '__main__':
    # Take user arguments from command line
    parser = argparse.ArgumentParser()
    parser.add_argument("inputFileName", help="Name of image to be read in")
    parser.add_argument("outputFileName", help="Name of file to write to")
    parser.add_argument("horizontalShrinkInPixels", help="Number of pixels to cut vertically")
    parser.add_argument("verticalShrinkInPixels", help="Number of pixels to cut horizontally")
    args = parser.parse_args()

    # old version only # read in the image and convert to RGB instead of BGR as default by cv2
    read_image = cv2.imread(args.inputFileName)
    #read_image = cv2.cvtColor(read_image, cv2.COLOR_BGR2RGB)

    # Show original image
    #pyplot.imshow(read_image)
    #pyplot.show()

    new_image = read_image.copy()
    # cut out all the horizontal pixels we don't need (this runs the seam vertically in the image)
    print('Running all vertical seam carves. This could take a while')
    for i in trange(int(args.horizontalShrinkInPixels)):
        new_image = remove_pixels(new_image, new_image.shape[0], new_image.shape[1])

    # cut out all the vertical pixels we don't need (this runs the seam horizontally in the image)
    new_image = imutils.rotate_bound(new_image, 90)
    print('Running all horizontal seam carves. This could take a while')
    for i in trange(int(args.verticalShrinkInPixels)):
        new_image = remove_pixels(new_image, new_image.shape[0], new_image.shape[1])
    # rotate the image back
    new_image = imutils.rotate_bound(new_image, 270)

    # Write out the new image
    cv2.imwrite(args.outputFileName, new_image)
    print('Done! Image written to', args.outputFileName)

    # Show the new image
    #pyplot.imshow(new_image)
    #pyplot.show()