# Image Seam Carving with Python

## Setup

This project requires Python3 along with a handful of libraries in order to work properly.

Once python is installed, install the required dependencies that are found in the requirements.txt file. This can be done with the following command.

> pip install -r requirements.txt

## Running the Script

Put an image that you would like to modify in the same directory as the seam_carving.py script and then run the seam_carving.py script. If confused about the runtime arguments run

> python seam_carving.py --help

or run the application with

> python seam_carving.py \<inputFileName> \<outputFilePath> \<horizontalShrinkInPixels> \<verticalShrinkInPixels>

## Example Outputs
> python seam_carving.py images/landscape1_before.jpg images/landscape1_after.jpg 400 300
![Input](images/landscape1_before.jpg)
![Output](images/landscape1_after.jpg)

> python seam_carving.py images/landscape2_before.jpg images/landscape2_after.jpg 150 75
![Input](images/landscape2_before.jpg)
![Output](images/landscape2_after.jpg)

> python seam_carving.py images/artwork1_before.png images/artwork1_after.png 400 200
![Input](images/artwork1_before.png)
![Output](images/artwork1_after.png)

> python seam_carving.py images/artwork2_before.jpg images/artwork2_after.jpg 50 300
![Input](images/artwork2_before.jpg)
![Output](images/artwork2_after.jpg)
#### Image Output